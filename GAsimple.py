#coding: utf-8

# este programa describe un algoritmo genetico simple
# los individuos trataran de imitar un chromosoma modelo
## chromosoma modelo: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

import random

modelo = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
long = 10
num_ind = 10
select = 3
m_rate = 0.2
f_rate = 0
generations = 100

print("Chromosoma Modelo: {0}".format(modelo))


def individuos(min, max):
    """
    Funcion es para crear individuos
    con valores aleatorios entre el
    1 y el 10
    """
    return [random.randint(min, max) for i in range(long)]
#print(individuos(1, 10))

def poblacion():
    """
    Funcion que se encarga de generar la
    poblacion de individuos, el numero de
    individuos esta dado por la variable
    num_ind
    """
    return [individuos(1, 10) for i in range(num_ind)]

#print(len(poblacion()))


def func_fitness(individuo):
    """
    funcion para evaluar el fitness
    de cada individuo, la funcion es
    comparar a cada individuo con el chromosoma
    modelo, indice por indice
    """
    fitness = 0
    for i in range(len(individuo)):
        if individuo[i] == modelo[i]:
            fitness += 1 # fitness ++, ++fitness, fitness = fitness + 1
    return fitness

# ind = individuos(1, 10)
# print(ind)
# print(func_fitness(ind))

def select_cross(population):
    """
    Esta funcion, hace el ranking de los individuos
    y selecciona a los mejores, despues realiza el
    cruce con los individuos seleccionados, y regresa
    la poblacion incluyendo a los nuevos individuos
    """
    mejor_puntuado = [(func_fitness(i), i) for i in population]# (1, 1)
    mejor_puntuado = [i[1] for i in sorted(mejor_puntuado)]# mejor_puntuado.sort()
    population = mejor_puntuado

    selected = mejor_puntuado[(len(mejor_puntuado)-select):]# esta linea selecciona a los mejores individuos

    for i in range(len(population)-select):
        punto = random.randint(1, long-1)
        parents = random.sample(selected, 2) # Se seleccionan a dos chromosomas como padres

        population[i][:punto] = parents[0][:punto]# se cruzan los genes de los individuos
        population[i][:punto] = parents[1][:punto]# se cruzan los genes de los individuos


    return population


def mutation(population):
    """
    Esta funcion se encarga de mutar los genes
    de los individuos seleccionados para la mutacion
    """
    for i in range(len(population)-select):
        if random.random() <= m_rate:
            punto = random.randint(1, long-1)
            nuevo_gen = random.randint(1, 10)

            while nuevo_gen == population[i][punto]:
                nuevo_gen = random.randint(1, 10)

            population[i][punto] = nuevo_gen

    return population



population = poblacion()
print("Poblacion de Individuos: {0}".format(population))

for i in range(generations):
    population = select_cross(population)
    population = mutation(population)

print("Poblacion Final: {0}".format(population))
